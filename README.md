# azcwr.org

This is the git repository conversion for the [azcwr.org](http://azcwr.org/) website.

## Staging Site

A CI (continuous integration) pipeline is setup on the [AZCWR GitLab Repo](https://gitlab.com/azcwr/azcwr.org/). It will render the results of any updates pushed to the master branch onto the [staging site](https://azcwr.gitlab.io/azcwr.org) in less than 5 minutes (assuming they build correctly).

## Building the Site

This website is setup using the [Nanoc](https://nanoc.ws/) static site generator. Nanoc is written in [Ruby](https://www.ruby-lang.org/en/).

In order to build the site locally you will need to:

1. Install [rbenv](https://github.com/rbenv/rbenv#user-content-groom-your-apps-ruby-environment-with-rbenv) or [RVM](https://rvm.io/).
2. Install a version of Ruby from one of the above.
3. Ensure that gem is [up to date](http://guides.rubygems.org/command-reference/#gem-update). `gem update --system`
4. Install [Bundler](https://bundler.io/#getting-started).
5. Run `bundle install`
6. To view the [local site](http://localhost:3000/) live run `bundle exec nanoc live`

## Initial Transfer Procedure

This site is based off of the AZCWR website as it appeared on 2017-06-12. It was pulled down from the live site with the following command:

```shell
$ wget -mkE http://azcwr.org/
```

Then it was processed with the `bin/azcwr-converter.rb` script.

The resulting files were converted to [HAML](http://haml.info/tutorial.html) and [SASS](http://sass-lang.com/guide) and were then grafted into the content directory of my [nanoc-site-template](https://gitlab.com/egrieco/nanoc-site-template) repository.
