# Necessary Redirects

This is the annotated output from the bin/azcwr-link-collector.rb script.

We need redirects for any internal pages that no longer exist.

## Previous Site

### Internal Links

about-azcwr.html
access-the-ranges.html
analysis-team.html
az-cyber-warfare-ranges.html
calendar.html
calendar/participation-on-site-at-the-range
careers.html
careers/current_openings.html
contact-us.html
donating-to-the-azcwr.html
eastside.html
edcational-materials.html
events.html
forensics.html
index.html
learning-resources.html
management-team.html
online-resources.html
outreach.html
participation-on-site-at-the-range.html
partners.html
remote-access-ranges.html
special-ranges.html
sponsors.html
standard-ranges.html
westside.html
what-is-happening-at-the-azcwr.html
wp-content/uploads/2016/10/Logo-Recruit-Bit-find-the-1.jpg

### External Links

http://gofundme.com/ezavwc
http://goo.gl/forms/Zm7dgwiy7l
http://home.azcwr.org
http://luckybitrecruiting.com
http://phoenix.issa.org/
http://realtimesteam.org/
http://SWCSE.com
http://www.azinfragard.org
http://www.azlabs.org/
http://www.evohop.com
http://www.guidestar.org/profile/46-2255512
http://www.recruitbit.co
http://www.riveracg.com
https://docs.google.com/forms/d/1hVmdfbEDxJaQ7R9xw3CpLfxwPcMLm0gwaqpKPEIpMZE/viewform
https://goo.gl/forms/20qjmkgXxXizYiAk1
https://isc2chapter-phoenix.org/
https://plus.google.com/communities/101424802438076017863
https://plus.google.com/communities/109709683526296474584
https://plus.google.com/u/0/communities/101424802438076017863
https://recruitbit.co/
https://sites.google.com/view/livesquare/who-are-we
https://wordpress.org/
https://www.cybersecuritycanyon.org/
https://www.gcu.edu/
https://www.google.com/maps/place/6030+S+Kent+St,+Mesa,+AZ+85212/@33.3053899,-111.6738559,17z/data=!3m1!4b1!4m5!3m4!1s0x872bad81f2aa4e0d:0x406e7b926b4836f1!8m2!3d33.3053899!4d-111.6716672
https://www.hacksplaining.com/
https://www.linkedin.com/company/arizona-cyber-warfare-range

### Misc Links

cdn-cgi/l/email-protection.html

## Current Site

### Internal Links

about/
about/analysis-team/
about/donating-to-the-azcwr/
about/forensics/
about/leadership-team/
about/partners/
about/sponsors/
contact-us/
on-site/
on-site/az01/
on-site/az02/
on-site/special-events/
remote/
remote/access/
remote/request-account/
remote/special-ranges/
resources/
resources/careers/
resources/outreach/

### External Links

http://home.azcwr.org
http://www.azcybertalent.com/
https://goo.gl/forms/20qjmkgXxXizYiAk1
https://nanoc.ws/
https://plus.google.com/communities/109709683526296474584
https://swcse.com/
