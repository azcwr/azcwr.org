# AZCWR Todo

This file is a simple todo list to make sure that we don't lose track of any of the remaining transitionary tasks for the migration to the new website.

## Site Migration

* collect links from current site and add redirect pages

## About Section

### About

* clean up page
* add images/cards

### Leadership Team

* is there anyone else we want to add?
* pictures?

### Analysis Team

* add call to action

### Forensics

* break page into sections or cards
* add call to action

### Partners

* clean up page
* add images/cards
* move donations text, it has its own page
* increase visibility of Embry-Riddle Cyber Security Club and the Mesa Community College Cyber Security Club thank you
* move Alien Vault under operational partners

### Sponsors

* add images/cards
* make PayPal link into a button or move it to donations page
* add call to action (sponsor signup link?)

### Donating to the AZCWR

* fix page title
* clean up page

## On Site Section

### On Site

* clean up page

### Participation On-Site

* clean up title
* clean up page

### Special Events

* clean up page
* add call to action

### East/West Range Pages

* normalize pages
* normalize titles in menu

### West Valley Range – AZ02

* when visiting the range as a guest
* valid escorts are

## Remote

* consolidate the standard ranges and access the ranges pages

### Remote

* clean up title
* clean up page
* add call to action

### Request Account

* add uptime and legal disclaimers or links to such

### Access the Ranges

* decide on final look for the range "cards" and access instructions

### Special Ranges

* add a call to action

## Resources

### Resources

* clean up page

### Careers

* clean up page or make it a subdomain

### Outreach

* clean up page
* add a call to action

## Contact Us

* add addresses (what is our official mailing address?)
