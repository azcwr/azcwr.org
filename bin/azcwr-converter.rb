# This is a one-off script to convert html pages in the current directory to haml files
# additionally, it only converts the parts of the pages in which we are interested
# it is intended to be run as:
# 
# $ cd /PATH/TO/azcwr.org/content
# $ ruby ../bin/azcwr-converter.rb

# library to parse, search and manipulate HTML and XML
require 'nokogiri'

# for html to haml conversion
require 'html2haml'
require 'html2haml/html'

# to create frontmatter attributes
require 'yaml'

# find all html files in the current directory
Dir.glob('*.html').each do |file|
  # parse the file into a Nokogiri document
  doc = Nokogiri::HTML(File.new(file).read)

  # setup a has for the page attributes
  page_attributes = {}

  # get the page title (using CSS style selectors)
  # we were pulling the title from the actual title tag, but it has "- Arizona Cyber Warfare Range" appended
  # page_attributes['title'] = doc.css('title').first.content
  page_attributes['title'] = doc.css('.entry-header .entry-title').first.content

  # get the page content (using CSS style selectors)
  original_page_content = doc.css('.entry-content').inner_html

  # convert the page content to haml
  processed_page_content = Html2haml::HTML.new(original_page_content).render

  # switch the output file extension
  output_file_name = [File::basename(file, '.html'), 'haml'].join('.')

  # write the output haml file
  STDERR.puts "#{file} -> #{output_file_name}"
  File.open(output_file_name, 'w') do |output_file|
    output_file.write page_attributes.to_yaml
    output_file.puts '---'
    output_file.write(processed_page_content)
  end
end
