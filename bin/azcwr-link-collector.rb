# This is a one-off script to collect all of the links from the html pages in the current directory
# it should:
#   1. collect all links
#   2. normalize links
#   3. deduplicate links
#   4. create and output file that allows easy creation of redirects to new, equivalent pages
#
# it is intended to be run as:
#
# $ cd /PATH/TO/azcwr.org/content
# $ ruby ../bin/azcwr-link-collector.rb

# library to parse, search and manipulate HTML and XML
require 'nokogiri'

links = []

# find all html files in the current directory
Dir.glob('*.html').each do |file|
  # parse the file into a Nokogiri document
  doc = Nokogiri::HTML(File.new(file).read)

  links += doc.css('a').map { |link| link[:href] }
end

STDERR.puts "#{links.length} total links found"
unique_links = links.uniq.sort

STDERR.puts "#{unique_links.length} unique links found"
puts unique_links.join("\n")
